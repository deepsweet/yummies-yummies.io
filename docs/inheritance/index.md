# Inheritance

Basically the way classes looks in Yummies is nothing different from React. For example, here is a piece of the `Input` component which we inherit from `React.component`:

```js
export default class Input extends React.component {
    constructor(props) {
        super(props);

        this.state = {
            value: props.value || '',
            focused: false,
            hovered: false
        };
    }

    // ...

    render() {
        return {
            block: 'input',
            tag: 'input',
            mods: {
                focused: this.state.focused,
                hovered: this.state.hovered,
                disabled: this.props.disabled || false
            },
            props: {
                ...this.props,
                value: this.state.value,
                onChange: this._onInputChange,
                onFocus: this._onInputFocus,
                onBlur: this._onInputBlur,
                onMouseLeave: this._onInputMouseLeave,
                onMouseEnter: this._onInputMouseEnter,
            }
        };
    }
}
```

Of course we can inherit from any other component and redefine any parent's class method or property.

## Extending .render()

When creating some component based on another we will probably wouldn't want to rewrite the whole `.render()` method but just make some little changes instead. Thanks to the **BEM JSON** we can do it easily just by adding more stuff into the parent _“template”_:

```js
export default class SearchInput extends Input {
    render() {
        let template = super.render();

        template.mods = {
            ...template.mods,
            type: 'search'
        };

        template.props = {
            ...template.props,
            type: 'search',
            autoComplete: 'off',
            autoCapitalize: 'off',
            autoCorrect: 'off',
            spellCheck: 'off'
        };

        return template;
    }
}
```

## Multiple inheritance (mixins)

If we want the component to be extended from more than one class, we can just use `mixins` property:

```js
export default class AdvancedInput extends Input {
    ...
    static get mixins() {
        return [ MixinValidate, MixinInputError ];
    }
    ...
}
```

So in this case we just take `MixinValidate` and `MixinInputError` (which are ES6 classes as well) and just put them into the inheritance chain, so it becomes:

```
Input -> MixinValidate -> MixinInputError -> AdvancedInput
```
