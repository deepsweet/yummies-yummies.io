# Layers

In Yummies we introduce the concept of **layers** which is very similar to [BEM levels](https://en.bem.info/tools/bem/bem-tools/levels/).

## Base class and inheritance chain

Before we get to the **magic** part, let's take a look at how we can not explicitly specify which class we are extending from. For example, take a look at the `Link` component:

```js
export default Base => class extends Base {
    render() {
        return {
            block: 'link',
            tag: 'a',
            props: this.props,
            content: this.props.children
        };
    }
}
```

You can notice that we inherit from abstract `Base` class (or name it whatever you want) which is by default `React.Component`. And what we actually export here is not the class itself but the result of the inheritance chain.

So when we want to inherit from another component we import it's *raw* instance with [Yummies inherit loader](https://github.com/yummies/inherit-loader) and pass the **Base** class to it. Let's inherit from `Link` component mentioned above:

```js
// AdvancedLink
import Link from '#link?raw&-styles';

export default Base => class extends Link(Base) {
    ...
}
```

In this case the inheritance chain will be:

```
Base (which is React.Component) -> Link -> AdvancedLink
```

## Layers of inheritance

Let's take a look at a typical use-case: we want a `Button` component in our app.

We can write it from scratch and stick all the functionality and styles into the one huge and complicated component that will be difficult to manage and reuse.

Or we can just split it into a few separate **layers** of inheritance chain. In our case it can be something like this:

1. **core** layer: the very basic functionality (element layout, focus/hover states, ability to track value changes)
2. **reset** layer: styles resetting so the `Button` will not have any major differences across the browsers
3. **theme** layer: styles for the *context* we are in (backgrounds, shadows, etc.)
4. **app** layer: app specific stuff (for example we can add *Validation* mixin or something like this)

So what do we do to achieve that? First of all we need to add the layers we want to use into the yummies-inherit-loader config:

```js
// ...
import path from 'path';

const yummiesConfig = {
    layers: [
        path.resolve('core/components/'),
        path.resolve('reset/components/'),
        path.resolve('theme/components/'),
        path.resolve('app/components/')
    ]
}
// ...
module: {
    loaders: [
        {
            test: /\.es6$/,
            loader: '@yummies/inherit-loader?' + JSON.stringify(yummiesConfig)
        }
    ]
}
// ...
```

Then we just import `Button` component in our **app** like this:

```js
import Button from '#button';
```

For every layer **Base** class, which we inherit from, is being replaced with the component of the previous layer. So for the **core Button** it's *React.Component* (because there is no previous layer), for the **reset Button** it's *core Button*, and so on up to he last layer.

After all the prototype chain looks like this:

```
React.Component -> core Button -> reset Button -> theme Button -> app Button
```

To illustrate it better take a look at this DEMO:

ТУТ ДЕМО СО СЛОЯМИ БЛЯТЬ
