## Mission

* find all the special `require('#…')`s (which starts with `#` symbol)
* run through `layers` dirs array (defined in config) and collect a CHAIN of existing files in special order
* rewrite original `require('#…')` with `require('yummies').yummify(CHAIN)`

## Examples

### Simple

file system:

```
.
└── src/
    ├── core-components/
    │   └── example/
    │       └── index.es6
    └── my-components/
        └── example/
            └── index.es6
```

config:

```js
import path from 'path';

export default {
    layers: [
        path.resolve('src/core-components/'),
        path.resolve('src/my-components/')
    ]
};
```

source:

```js
import Example from '#example';
```

result:

```js
var Example = require('yummies').yummify([
    {
        type: 'main',
        module: require('./src/core-components/example/index.es6')
    },
    {
        type: 'main',
        module: require('./src/my-components/example/index.es6')
    }
]);
```

### With styles

file system:

```sh
.
└── src/
    ├── core-components/
    │   └── example/
    │       ├── index.es6
    │       └── styles.less
    └── my-components/
        └── example/
            ├── index.es6
            └── styles.less
```

config:

```js
{
    styles: 'styles.less',
    layers: [
        path.resolve('src/core-components/'),
        path.resolve('src/my-components/')
    ]
}
```

source:

```js
import Example from '#example';
```

result:

```js
var Example = require('yummies').yummify([
    {
        type: 'main',
        module: require('./src/core-components/example/index.es6')
    },
    {
        type: 'styles',
        module: require('./src/core-components/example/styles.less')
    },
    {
        type: 'main',
        module: require('./src/my-components/example/index.es6')
    },
    {
        type: 'styles',
        module: require('./src/my-components/example/styles.less')
    }
]);
```

### With propTypes

file system:

```sh
.
└── src/
    ├── core-components/
    │   └── example/
    │       ├── index.es6
    │       └── prop-types.es6
    └── my-components/
        └── example/
            ├── index.es6
            └── prop-types.es6
```

config:

```js
import path from 'path';

export default {
    propTypes: 'prop-types.es6',
    layers: [
        path.resolve('src/core-components/'),
        path.resolve('src/my-components/')
    ]
};
```

source:

```js
import Example from '#example';
```

result:

```js
var Example = require('yummies').yummify([
    {
        type: 'main',
        module: require('./src/core-components/example/index.es6')
    },
    {
        type: 'propTypes',
        module: require('./src/core-components/example/prop-types.es6')
    },
    {
        type: 'main',
        module: require('./src/my-components/example/index.es6')
    },
    {
        type: 'propTypes',
        module: require('./src/my-components/example/prop-types.es6')
    }
]);
```

### Mods

file system:

```sh
.
└── src/
    ├── core-components/
    │   └── example/
    │       ├── _type/
    │       │   └── test/
    │       │       └── index.es6
    │       ├── index.es6
    │       └── styles.less
    └── my-components/
        └── example/
            ├── _type/
            │   └── test/
            │       └── styles.less
            ├── index.es6
            └── styles.less
```

config:

```js
import path from 'path';

export default {
    styles: 'styles.less',
    layers: [
        path.resolve('src/core-components/'),
        path.resolve('src/my-components/')
    ]
};
```

source:

```js
import ExampleTypeTest from '#example?_type=test';
```

result:

```js
var ExampleTypeTest = require('yummies').yummify([
    {
        type: 'main',
        module: require('./src/core-components/example/index.es6')
    },
    {
        type: 'styles',
        module: require('./src/core-components/example/styles.less')
    },
    {
        type: 'main',
        module: require('./src/my-components/example/index.es6')
    },
    {
        type: 'styles',
        module: require('./src/my-components/example/styles.less')
    },
    {
        type: 'main',
        module: require('./src/core-components/example/_type/test/index.es6')
    },
    {
        type: 'styles',
        module: require('./src/my-components/example/_type/test/styles.less')
    }
]);
```

### Multiple mods

The same process as for single mod:

```js
import ExampleTypeTestSizeBig from '#example?_type=test&_size=big';
```

### Ignore styles

```js
import ExampleIndexOnly from '#example?-styles';
```

### "Raw" (invoke `yummifyRaw()`)

```js
import ExampleRaw from '#example?raw';
```
