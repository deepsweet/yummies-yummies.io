import Yummies from '@yummies/yummies';
import content from 'docs/index/';

import './styles.less';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/index';

    render() {
        return {
            block: 'page',
            elem: 'index',
            props: {
                dangerouslySetInnerHTML: {
                    __html: content
                }
            }
        };
    }
});
