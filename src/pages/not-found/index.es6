import Yummies from '@yummies/yummies';
import content from 'docs/not-found/';

import './styles.less';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/not-found';

    render() {
        return {
            block: 'page',
            elem: 'not-found',
            props: {
                dangerouslySetInnerHTML: {
                    __html: content
                }
            }
        };
    }
});
