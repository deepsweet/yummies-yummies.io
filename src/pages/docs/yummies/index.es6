import Yummies from '@yummies/yummies';
import content from 'docs/yummies/';

import './styles.less';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/docs/yummies';

    render() {
        return {
            block: 'page',
            elem: 'yummies',
            props: {
                dangerouslySetInnerHTML: {
                    __html: content
                }
            }
        };
    }
});
