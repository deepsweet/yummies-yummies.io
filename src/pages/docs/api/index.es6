import Yummies from '@yummies/yummies';
import content from 'docs/api/';

import './styles.less';

import MarkdownBody from '#markdown-body';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/docs/api';

    render() {
        return {
            block: 'page',
            elem: 'api',
            content: MarkdownBody({
                dangerouslySetInnerHTML: {
                    __html: content
                }
            })
        };
    }
});
