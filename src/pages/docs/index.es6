import Yummies from '@yummies/yummies';
import { RouteHandler } from 'react-router';

import './styles.less';

const RouteHandlerFactory = Yummies.createFactory(RouteHandler);

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/docs';

    render() {
        return {
            block: 'page',
            elem: 'docs',
            content: RouteHandlerFactory()
        };
    }
});
