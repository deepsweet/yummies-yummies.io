import Yummies from '@yummies/yummies';
import content from 'docs/inheritance/';

import './styles.less';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/docs/inheritance';

    render() {
        return {
            block: 'page',
            elem: 'inheritance',
            props: {
                dangerouslySetInnerHTML: {
                    __html: content
                }
            }
        };
    }
});
