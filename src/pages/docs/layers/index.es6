import Yummies from '@yummies/yummies';
import content from 'docs/layers/';

import './styles.less';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/docs/layers';

    render() {
        return {
            block: 'page',
            elem: 'layers',
            props: {
                dangerouslySetInnerHTML: {
                    __html: content
                }
            }
        };
    }
});
