import Yummies from '@yummies/yummies';
import content from 'docs/render/';

import './styles.less';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/docs/render';

    render() {
        return {
            block: 'page',
            elem: 'render',
            props: {
                dangerouslySetInnerHTML: {
                    __html: content
                }
            }
        };
    }
});
