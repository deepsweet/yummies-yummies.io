import Yummies from '@yummies/yummies';
import content from 'docs/loader/';

import './styles.less';

import MarkdownBody from '#markdown-body';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/docs/loader';

    render() {
        return {
            block: 'page',
            elem: 'loader',
            content: MarkdownBody({
                dangerouslySetInnerHTML: {
                    __html: content
                }
            })
        };
    }
});
