import Yummies from '@yummies/yummies';
import content from 'docs/status/';

import './styles.less';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/status';

    render() {
        return {
            block: 'page',
            elem: 'status',
            props: {
                dangerouslySetInnerHTML: {
                    __html: content
                }
            }
        };
    }
});
