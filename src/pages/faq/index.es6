import Yummies from '@yummies/yummies';
import content from 'docs/faq/';

import './styles.less';

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'pages/faq';

    render() {
        return {
            block: 'page',
            elem: 'faq',
            props: {
                dangerouslySetInnerHTML: {
                    __html: content
                }
            }
        };
    }
});
