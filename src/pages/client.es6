import React from 'react';
import nextTick from 'next-tick';

function getJSmodule(moduleID, callback) {
    /* global webpackJsonp */
    webpackJsonp([], {
        0: function(module, exports, require) {
            callback(require(moduleID));
        }
    });
}

function loadJS(name, pageID) {
    const filename = '/js/' + name + '.js';

    return new Promise(resolve => {
        nextTick(() => {
            const script = document.createElement('script');
            const scripts = document.scripts;

            for (let i = 0, l = scripts.length; i < l; i++) {
                if (
                    scripts[i].src &&
                    scripts[i].src.indexOf(filename, l - filename.length) !== -1
                ) {
                    getJSmodule(pageID, resolve);

                    return;
                }
            }

            script.src = filename;
            script.async = true;
            script.onload = function() {
                getJSmodule(pageID, resolve);
            };

            document.head.appendChild(script);
        });
    });
}

function loadCSS(name) {
    const filename = '/css/' + name + '.css';
    const styles = document.styleSheets;

    for (let i = 0, l = styles.length; i < l; i++) {
        if (
            styles[i].href &&
            styles[i].href.indexOf(filename, l - filename.length) !== -1
        ) {
            return;
        }
    }

    return new Promise(resolve => {
        const link = document.createElement('link');

        link.rel = 'stylesheet';
        link.href = filename;
        link.onload = resolve;
        link.onerror = resolve;

        document.head.appendChild(link);
    });
}

function createAsyncHandler(names, pageID) {
    let Handler = null;

    return React.createClass({
        statics: {
            willTransitionTo(transition, params, query, callback) {
                const promisesToWait = [];
                let filename = 'page';

                names.forEach((name, index) => {
                    filename += '-' + name;

                    if (index === names.length - 1) {
                        promisesToWait.push(loadCSS(filename));

                        promisesToWait.push(loadJS(filename, pageID).then(result => {
                            Handler = result;
                        }));
                    }
                });

                Promise.all(promisesToWait).then(() => {
                    if (!Handler.willTransitionTo) {
                        return callback();
                    }

                    Handler.willTransitionTo(transition, params, query, callback);

                    if (Handler.willTransitionTo.length < 4) {
                        callback();
                    }
                });
            },

            willTransitionFrom(transition, component, callback) {
                if (!Handler || !Handler.willTransitionFrom) {
                    return callback();
                }

                Handler.willTransitionFrom(transition, component, callback);

                if (Handler.willTransitionFrom.length < 3) {
                    callback();
                }
            }
        },

        render() {
            return React.createElement(Handler, this.props);
        }
    });
}

export default {
    index: createAsyncHandler([ 'index' ], require.resolveWeak('./index/')),
    docs: createAsyncHandler([ 'docs' ], require.resolveWeak('./docs/')),
    'docs-yummies': createAsyncHandler([ 'docs', 'yummies' ], require.resolveWeak('./docs/yummies/')),
    'docs-render': createAsyncHandler([ 'docs', 'render' ], require.resolveWeak('./docs/render/')),
    'docs-inheritance': createAsyncHandler([ 'docs', 'inheritance' ], require.resolveWeak('./docs/inheritance/')),
    'docs-layers': createAsyncHandler([ 'docs', 'layers' ], require.resolveWeak('./docs/layers/')),
    'docs-loader': createAsyncHandler([ 'docs', 'loader' ], require.resolveWeak('./docs/loader/')),
    'docs-api': createAsyncHandler([ 'docs', 'api' ], require.resolveWeak('./docs/api/')),
    status: createAsyncHandler([ 'status' ], require.resolveWeak('./status/')),
    faq: createAsyncHandler([ 'faq' ], require.resolveWeak('./faq/')),
    'not-found': createAsyncHandler([ 'not-found' ], require.resolveWeak('./not-found/'))
};
