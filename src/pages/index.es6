import Yummies from '@yummies/yummies';
import { RouteHandler } from 'react-router';
import Header from '#header';
import Footer from '#footer';

const RouteHandlerFactory = Yummies.createFactory(RouteHandler);

export default Yummies._prepareClass(class extends Yummies.Component {
    static displayName = 'page';

    render() {
        return {
            block: 'page',
            content: [
                Header({
                    key: 'header'
                }),
                RouteHandlerFactory({
                    key: 'route'
                }),
                Footer({
                    key: 'footer'
                })
            ]
        };
    }
});
