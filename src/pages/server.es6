import pageIndex from './index/';
import pageDocs from './docs/';
import pageDocsYummies from './docs/yummies/';
import pageDocsRender from './docs/render/';
import pageDocsInheritance from './docs/inheritance/';
import pageDocsLayers from './docs/layers/';
import pageDocsLoader from './docs/loader/';
import pageDocsAPI from './docs/api/';
import pageStatus from './status/';
import pageFaq from './faq/';
import pageNotFound from './not-found/';

export default {
    index: pageIndex,
    docs: pageDocs,
    'docs-yummies': pageDocsYummies,
    'docs-render': pageDocsRender,
    'docs-inheritance': pageDocsInheritance,
    'docs-layers': pageDocsLayers,
    'docs-loader': pageDocsLoader,
    'docs-api': pageDocsAPI,
    status: pageStatus,
    faq: pageFaq,
    'not-found': pageNotFound
};
