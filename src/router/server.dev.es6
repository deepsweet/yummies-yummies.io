import React from 'react';
import Yummies from '@yummies/yummies';
import Router from 'react-router';
import Html from '#html';

import routesFabric from './routes';
import pages from '../pages/server';

export default function(url) {
    return function(callback) {
        Router.run(routesFabric(pages), url, Handler => {
            const html = Yummies.renderToStaticMarkup(
                Html({
                    _title: 'test',
                    _markup: Yummies.renderToString(React.createElement(Handler)),
                    _js: [ 'http://localhost:3001/bundle.js' ]
                })
            );

            callback(null, '<!DOCTYPE html>' + html);
        });
    };
}
