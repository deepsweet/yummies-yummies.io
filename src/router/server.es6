import Yummies from '@yummies/yummies';
import Router from 'react-router';

import routesFabric from './routes';
import pages from '../pages/server';

import Html from '#html';

function generatePaths(routes, tech) {
    let fileName = 'page';

    return routes.map(route => {
        fileName += '-' + route.name;

        return '/' + tech + '/' + fileName + '.' + tech;
    });
}

export default function(url) {
    return function(callback) {
        Router.run(routesFabric(pages), url, (Handler, state) => {
            const routes = state.routes.slice(1);

            const html = Yummies.renderToStaticMarkup(
                Html({
                    _title: 'test',
                    _markup: Yummies.renderToString(Yummies.createElement(Handler)),
                    _css: [
                        '/css/vendor.css',
                        '/css/common.css',
                        ...generatePaths(routes, 'css')
                    ],
                    _js: [
                        '/js/vendor.js',
                        '/js/common.js',
                        ...generatePaths(routes, 'js')
                    ]
                })
            );

            callback(null, '<!DOCTYPE html>' + html);
        });
    };
}
