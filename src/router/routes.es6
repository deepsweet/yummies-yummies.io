import React from 'react';
import Router from 'react-router';

import WrapperHandler from '../pages/';

const Route = React.createFactory(Router.Route);
const DefaultRoute = React.createFactory(Router.DefaultRoute);
const NotFoundRoute = React.createFactory(Router.NotFoundRoute);

export default function(pages) {
    return Route(
        {
            name: 'wrapper',
            path: '/',
            handler: WrapperHandler
        },
        DefaultRoute({
            name: 'index',
            handler: pages.index
        }),
        Route(
            {
                name: 'docs',
                path: 'docs/',
                handler: pages.docs
            },
            DefaultRoute({
                name: 'yummies',
                handler: pages['docs-yummies']
            }),
            Route({
                name: 'render',
                path: 'render/',
                handler: pages['docs-render']
            }),
            Route({
                name: 'inheritance',
                path: 'inheritance/',
                handler: pages['docs-inheritance']
            }),
            Route({
                name: 'layers',
                path: 'layers/',
                handler: pages['docs-layers']
            }),
            Route({
                name: 'loader',
                path: 'loader/',
                handler: pages['docs-loader']
            }),
            Route({
                name: 'api',
                path: 'api/',
                handler: pages['docs-api']
            })
        ),
        Route({
            name: 'status',
            path: 'status/',
            handler: pages.status
        }),
        Route({
            name: 'faq',
            path: 'faq/',
            handler: pages.faq
        }),
        NotFoundRoute({
            handler: pages['not-found']
        })
    );
}
