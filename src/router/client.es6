import React from 'react';
import Router from 'react-router';

import routesFabric from './routes';
import pages from '../pages/client';

Router.run(routesFabric(pages), Router.HistoryLocation, Handler => {
    React.render(
        React.createElement(Handler),
        document.getElementById('app')
    );
});
