export default Base => class extends Base {
    static displayName = 'Footer';

    onFooterClick() {
        console.log('footer click');
    }

    render() {
        return {
            block: 'footer',
            props: {
                ...this.props,
                onClick: this.onFooterClick
            },
            content: 'footer'
        };
    }
};
