export default Base => class extends Base {
    static displayName = 'Html';

    _buildCSS() {
        if (!('_css' in this.props)) {
            return [];
        }

        return this.props._css.map(href => {
            return {
                tag: 'link',
                props: {
                    href,
                    rel: 'stylesheet',
                    key: 'styles-' + href
                }
            };
        });
    }

    _buildJS() {
        if (!('_js' in this.props)) {
            return [];
        }

        return this.props._js.map(src => {
            return {
                tag: 'script',
                props: {
                    src,
                    key: 'script-' + src
                }
            };
        });
    }

    render() {
        return {
            tag: 'html',
            content: [
                {
                    tag: 'head',
                    props: {
                        key: 'head'
                    },
                    content: [
                        {
                            tag: 'title',
                            props: {
                                key: 'title'
                            },
                            content: this.props._title
                        },
                        {
                            tag: 'link',
                            props: {
                                id: 'favicon',
                                rel: 'icon',
                                key: 'favicon'
                            }
                        },
                        ...this._buildCSS()
                    ]
                },
                {
                    tag: 'body',
                    props: {
                        key: 'body'
                    },
                    content: [
                        {
                            block: 'app',
                            props: {
                                id: 'app',
                                dangerouslySetInnerHTML: {
                                    __html: this.props._markup
                                },
                                key: 'app'
                            }
                        },
                        ...this._buildJS()
                    ]
                }
            ]
        };
    }
};
