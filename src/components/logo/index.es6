export default Base => class extends Base {
    static displayName = 'Logo';

    render() {
        return {
            block: 'logo',
            mods: {
                icon: this.props._icon
            },
            props: this.props,
            content: this.props.children
        };
    }
};
