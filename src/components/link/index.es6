import Yummies from '@yummies/yummies';
import React from 'react';
import Router from 'react-router';

const RouterLink = React.createFactory(Router.Link);

export default Base => class extends Base {
    static displayName = 'app: link';

    render() {
        const template = super.render();

        if (this.props._route) {
            return RouterLink({
                to: this.props._route,
                className: Yummies.buildClassName({
                    ...template,
                    ...this.props
                }),
                props: this.props
            }, this.props.children);
        }

        return template;
    }
};
