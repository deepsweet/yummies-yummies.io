export default Base => class extends Base {
    static displayName = 'MarkdownBody';

    render() {
        return {
            block: 'markdown-body',
            props: this.props
        };
    }
};
