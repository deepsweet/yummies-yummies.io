import Favicon from '#favicon';
import Menu from '#menu';
import Link from '#link';
import Logo from '#logo';

export default Base => class extends Base {
    static displayName = 'Header';

    constructor(props) {
        super(props);

        this.state = {
            logo: 'watermelon-slice'
        };
    }

    render() {
        return {
            block: 'header',
            props: this.props,
            content: [
                Favicon({
                    _icon: this.state.logo,
                    _faviconDomId: 'favicon',
                    key: 'favicon'
                }),
                Link(
                    {
                        _route: 'index',
                        mix: {
                            block: 'header',
                            elem: 'logo-link'
                        },
                        key: 'logo'
                    },
                    Logo({
                        _icon: this.state.logo,
                        mix: {
                            block: 'header',
                            elem: 'logo'
                        }
                    })
                ),
                Menu({
                    _links: [
                        Link({ _route: 'render' }, 'render'),
                        Link({ _route: 'inheritance' }, 'inheritance'),
                        Link({ _route: 'layers' }, 'layers'),
                        Link({ _route: 'loader' }, 'loader'),
                        Link({ _route: 'api' }, 'API'),
                        Link({ _route: 'status' }, 'status'),
                        Link({ _route: 'faq' }, 'faq')
                    ],
                    mods: {
                        type: 'main'
                    },
                    mix: {
                        block: 'header',
                        elem: 'main-menu'
                    },
                    key: 'main-menu'
                }),
                // TODO move to this structure
                // Menu({
                //     _links: [
                //         Link({ href: 'docs' }, 'docs'),
                //             // Link({ _route: 'bem' }, 'BEM syntax'),
                //             // Link({ _route: 'inheritance' }, 'Inheritance'),
                //             // Link({ _route: 'layers' }, 'Layers'),
                //         Link({ href: 'tools' }, 'tools'),
                //             // Link({ _route: 'yummies' }, 'Yummies render'),
                //             // Link({ _route: 'babel' }, 'Babel plugin'),
                //             // Link({ _route: 'styles-loader' }, 'Common styles loader'),
                //             // Link({ _route: 'themes-components' }, 'Components and themes'),
                //             // Link({ _route: 'starter-kit' }, 'Starter kit'),
                //         Link({ _route: 'status' }, 'status'),
                //         Link({ _route: 'faq' }, 'faq')
                //     ],
                //     mods: {
                //         type: 'main'
                //     },
                //     mix: {
                //         block: 'header',
                //         elem: 'main-menu'
                //     },
                //     key: 'main-menu'
                // }),
                Menu({
                    _links: [
                        Link({ href: 'https://github.com/yummies/' }, 'github'),
                        Link({ href: 'https://gitter.im/yummies/yummies/' }, 'gitter'),
                        Link({ href: 'https://twitter.com/yummies/' }, 'twitter')
                    ],
                    mods: {
                        type: 'links'
                    },
                    mix: {
                        block: 'header',
                        elem: 'links-menu'
                    },
                    key: 'links-menu'
                })
            ]
        };
    }
};
