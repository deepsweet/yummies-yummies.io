export default Base => class extends Base {
    static displayName = 'Menu';

    getMenuItems() {
        if (!('_links' in this.props)) {
            return null;
        }

        return this.props._links.map(link => {
            return {
                elem: 'item',
                tag: 'li',
                props: {
                    key: link.props._route || link.props.href
                },
                content: link
            };
        });
    }

    render() {
        return {
            block: 'menu',
            tag: 'ul',
            props: this.props,
            content: this.getMenuItems()
        };
    }
};
