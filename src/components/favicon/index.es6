const icons = {
    'watermelon-slice': require('./watermelon-slice.png')
};

export default Base => class extends Base {
    static displayName = 'Favicon';

    constructor(props) {
        super(props);

        this.favicon = null;
    }

    componentDidMount() {
        if (this.props._faviconDomId) {
            this.favicon = document.getElementById(this.props._faviconDomId);
            this._updateFavicon(this.props._icon);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props._icon !== nextProps._icon) {
            this.updateFavicon(nextProps._icon);
        }
    }

    _updateFavicon(icon) {
        if (this.favicon !== null && icon in icons) {
            this.favicon.href = icons[icon];
        }
    }

    render() {
        return null;
    }
};
