import fs from 'fs';
import path from 'path';
import koa from 'koa';
import koaResponseTime from 'koa-response-time';
import koaLogger from 'koa-logger';
import koaHelmet from 'koa-helmet';
import koaStaticCache from 'koa-static-cache';

import serverRender from './build/server/js/app';

const app = koa();

// https://github.com/koajs/response-time
app.use(koaResponseTime());

// https://github.com/koajs/logger
app.use(koaLogger());

// https://github.com/venables/koa-helmet
app.use(koaHelmet.defaults());

// is there anything to serve?
const assetsPath = path.resolve('build/client/');

if (fs.existsSync(assetsPath)) {
    app.use(koaStaticCache(assetsPath), {
        buffer: true
    });
}

// trailing slash
app.use(function*(next) {
    if (this.method !== 'POST' && this.url.substr(-1) !== '/') {
        this.status = 308;

        return this.redirect(this.url + '/');
    }

    yield next;
});

// server-side render
app.use(function*() {
    this.body = yield serverRender(this.request.url);
});

app.listen(3000);

console.log('http://localhost:3000/');
