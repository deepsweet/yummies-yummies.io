Yummies.io (WIP)
=====================

[![Stories in Ready](https://badge.waffle.io/yummies/yummies.io.png?label=ready&title=Ready)](https://waffle.io/yummies/yummies.io)

## build

```sh
npm start
open http://localhost:3000/
```

## dev

```sh
npm run dev
```

```sh
npm run server
open http://localhost:3000/
```

## prod

```sh
npm run prod
```
