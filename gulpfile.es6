import gulp from 'gulp';

import './gulp/clean';
import './gulp/webpack';
import './gulp/minify';

gulp.task('build',
    gulp.series(
        'clean:build:client',
        'webpack:build:client',
        'clean:build:server',
        'webpack:build:server'
    )
);

gulp.task('dev',
    gulp.series(
        'clean:build:client',
        'webpack:dev:client',
        'clean:build:server',
        'webpack:dev:server'
    )
);

gulp.task('minify', gulp.series('minify:build'));

gulp.task('default', gulp.series('build'));
