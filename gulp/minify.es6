import gulp from 'gulp';

gulp.task('minify:build:js', () => {
    const uglify = require('gulp-uglify');
    const rename = require('gulp-rename');

    const dir = 'build/client/js/';

    return gulp.src(dir + '*.js')
        .pipe(uglify({
            mangle: true,
            compress: { warnings: false }
        }))
        .pipe(rename(path => {
            path.basename += '.min';
        }))
        .pipe(gulp.dest(dir));
});

gulp.task('minify:build:css', () => {
    const minifyCSS = require('gulp-minify-css');
    const rename = require('gulp-rename');

    const dir = 'build/client/css/';

    return gulp.src(dir + '*.css')
        .pipe(minifyCSS({
            keepSpecialComments: 0,
            noRebase: true,
            processImport: false
        }))
        .pipe(rename(path => {
            path.basename += '.min';
        }))
        .pipe(gulp.dest(dir));
});

gulp.task('minify:build:images', () => {
    const imagemin = require('gulp-imagemin');

    return gulp.src('src/images/*')
        .pipe(imagemin({
            optimizationLevel: 7,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('build/images/'));
});

gulp.task('minify:build',
    gulp.parallel(
        'minify:build:js',
        'minify:build:css',
        'minify:build:images'
    )
);
