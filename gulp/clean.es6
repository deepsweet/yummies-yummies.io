import gulp from 'gulp';

gulp.task('clean:build:server', done => {
    const del = require('del');

    del('build/server/', done);
});

gulp.task('clean:build:client', done => {
    const del = require('del');

    del('build/client/', done);
});
