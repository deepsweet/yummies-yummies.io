import gulp from 'gulp';

const statsOptions = {
    colors: true,
    children: false,
    assets: false,
    version: false,
    hash: false,
    chunkModules: false
};

function build(type, platform, done) {
    // const fs = require('fs');
    // const path = require('path');
    const gutil = require('gulp-util');
    const webpack = require('webpack');
    const webpackBuildConfig = require('../conf/webpack.' + type + '.' + platform);

    webpack(webpackBuildConfig, (err, stats) => {
        if (err) {
            throw new gutil.PluginError('webpack:' + type + ':' + platform, err);
        }

        gutil.log('[webpack:' + type + ':' + platform + ']', stats.toString(statsOptions));

        done();

        // fs.writeFile(
        //     path.resolve('./build/stats.json'),
        //     JSON.stringify(stats.toJson({ source: false })),
        //     done
        // );
    });
}

gulp.task('webpack:build:server', done => {
    build('build', 'server', done);
});

gulp.task('webpack:build:client', done => {
    build('build', 'client', done);
});

gulp.task('webpack:dev:server', done => {
    const gutil = require('gulp-util');
    const webpack = require('webpack');
    const webpackConfig = require('../conf/webpack.dev.server');

    webpack(webpackConfig).watch(300, (err, stats) => {
        if (err) {
            throw new gutil.PluginError('webpack:dev:server', err);
        }

        gutil.log('[webpack:dev:server]', stats.toString(statsOptions));

        done();
    });
});

gulp.task('webpack:dev:client', done => {
    const gutil = require('gulp-util');
    const webpack = require('webpack');
    const WebpackDevServer = require('webpack-dev-server');
    const webpackDevConfig = require('../conf/webpack.dev.client');

    const server = new WebpackDevServer(webpack(webpackDevConfig), {
        hot: true,
        stats: statsOptions
    });

    server.listen('3001', 'localhost', err => {
        if (err) {
            throw new gutil.PluginError('webpack:dev:client', err);
        }

        done();
    });
});
