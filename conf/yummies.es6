import path from 'path';

export default {
    styles: 'styles.less',
    layers: [
        path.resolve('node_modules/@yummies/core-components/components'),
        path.resolve('src/components/')
    ]
};
