import webpack from 'webpack';
// import ProgressPlugin from 'webpack/lib/ProgressPlugin';

import webpackCommonConfig from './webpack.common';
import autoprefixerConfig from './autoprefixer';
import yummiesConfig from './yummies';

export default {
    ...webpackCommonConfig,
    entry: [
        './src/router/client.dev',
        'github-markdown-css/github-markdown.css',
        'highlight.js/styles/github.css',
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:3001'
    ],
    output: {
        ...webpackCommonConfig.output,
        path: '/',
        publicPath: 'http://localhost:3001/',
        filename: 'bundle.js'
    },
    module: {
        preLoaders: webpackCommonConfig.module.preLoaders,
        loaders: [
            ...webpackCommonConfig.module.loaders,
            {
                test: /\.less$/,
                loaders: [
                    'style',
                    'css?-minimize',
                    'autoprefixer?' + autoprefixerConfig,
                    'less',
                    '@yummies/common-styles-loader?' + JSON.stringify(yummiesConfig)
                ]
            },
            {
                test: /\.(svg|png)$/,
                loader: 'url?limit=1000'
            },
            {
                test: /\.css$/,
                loaders: [
                    'style',
                    'css?-minimize',
                    'autoprefixer?' + autoprefixerConfig
                ]
            }
        ]
    },
    plugins: [
        ...webpackCommonConfig.plugins,
        new webpack.HotModuleReplacementPlugin()
        // new ProgressPlugin((percentage, msg) => {
        //     percentage = Math.floor(percentage * 100);
        //     msg = percentage + '% ' + msg;
        //
        //     if (percentage < 10) msg = ' ' + msg;
        //
        //     process.stdout.clearLine();
        //     process.stdout.cursorTo(0);
        //     process.stdout.write(msg);
        // })
    ]
};
