export default JSON.stringify({
    browsers: [
        'last 2 Chrome versions',
        'last 2 Firefox versions'
    ]
});
