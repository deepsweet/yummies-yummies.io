import fs from 'fs';
import path from 'path';
import webpack from 'webpack';

import webpackCommonConfig from './webpack.common';

export default {
    ...webpackCommonConfig,
    target: 'node',
    entry: {
        app: './src/router/server'
    },
    output: {
        ...webpackCommonConfig.output,
        path: path.resolve('./build/'),
        filename: 'server/js/[name].js',
        libraryTarget: 'commonjs2'
    },
    externals: fs.readdirSync('node_modules').map(file => new RegExp('^' + file)),
    plugins: [
        ...webpackCommonConfig.plugins,
        new webpack.NormalModuleReplacementPlugin(/\.(less|svg|png)$/, 'node-noop')
    ]
    // profile: true
};
