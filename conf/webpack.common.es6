import path from 'path';
import webpack from 'webpack';

export default {
    cache: true,
    stats: {
        colors: true,
        reasons: false
    },
    output: {
        pathinfo: true
    },
    resolve: {
        root: path.resolve('src'),
        alias: {
            docs: path.resolve('docs')
        },
        extensions: [ '', '.js', '.es6', '.json', '.md' ]
    },
    module: {
        preLoaders: [
            {
                test: /\.es6$/,
                loader: 'babel',
                query: {
                    cacheDirectory: true
                }
            }
        ],
        loaders: [
            {
                test: /\.json$/,
                loader: 'json'
            },
            {
                test: /\.md$/,
                loader: 'html!markdown-highlight'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        })
    ]
};
