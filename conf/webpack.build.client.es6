import path from 'path';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
// import ChunkManifestPlugin from 'chunk-manifest-webpack-plugin';

import webpackCommonConfig from './webpack.common';
import autoprefixerConfig from './autoprefixer';
import yummiesConfig from './yummies';

export default {
    ...webpackCommonConfig,
    entry: {
        vendor: [
            'react',
            'react-router',
            '@yummies/yummies',
            'next-tick',
            'github-markdown-css/github-markdown.css',
            'highlight.js/styles/github.css'
        ],
        common: [ './src/router/client' ],
        'page-index': [ './src/pages/index/' ],
        'page-docs': [ './src/pages/docs/' ],
        'page-docs-yummies': [ './src/pages/docs/yummies' ],
        'page-docs-render': [ './src/pages/docs/render' ],
        'page-docs-inheritance': [ './src/pages/docs/inheritance' ],
        'page-docs-layers': [ './src/pages/docs/layers' ],
        'page-docs-loader': [ './src/pages/docs/loader' ],
        'page-status': [ './src/pages/status/' ],
        'page-faq': [ './src/pages/faq/' ],
        'page-not-found': [ './src/pages/not-found/' ]
    },
    output: {
        ...webpackCommonConfig.output,
        path: path.resolve('./build/client/'),
        filename: 'js/[name].js'
    },
    module: {
        preLoaders: webpackCommonConfig.module.preLoaders,
        loaders: [
            ...webpackCommonConfig.module.loaders,
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract(
                    'style',
                    'css?-minimize' +
                    '!autoprefixer?' + autoprefixerConfig +
                    '!less' +
                    '!@yummies/common-styles-loader?' + JSON.stringify(yummiesConfig)
                )
            },
            {
                test: /\.(svg|png)$/,
                loader: 'url?limit=100&name=/images/[hash].[ext]'
            },

            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract(
                    'style',
                    'css?-minimize'
                )
            }
        ]
    },
    plugins: [
        ...webpackCommonConfig.plugins,
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'js/[name].js'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common',
            filename: 'js/[name].js',
            chunks: [
                'page-index',
                'page-docs',
                'page-status',
                'page-faq',
                'page-not-found'
            ]
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'page-docs',
            filename: 'js/[name].js',
            chunks: [
                'page-docs-yummies',
                'page-docs-render',
                'page-docs-inheritance',
                'page-docs-layers',
                'page-docs-loader'
            ]
        }),
        new ExtractTextPlugin('css/[name].css', {
            allChunks: true
        }),
        new webpack.optimize.DedupePlugin()
        // new ChunkManifestPlugin()
    ]
};
